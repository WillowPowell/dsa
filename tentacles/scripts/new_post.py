import os
import datetime
import click
import yaml

from simple_term_menu import TerminalMenu

from tentacles.utils.user_input import rlinput
from tentacles.config import config

@click.command()
def new_post():
    category_path = config.category_path(choose_category())
    with open(config.METADATA_TEMPLATE) as f:
        template_metadata = yaml.safe_load(f.read())
    result = process_metadata_dict(template_metadata)
    result["created_at"] = datetime.datetime.now()

    post_folder = f"{category_path}/{result['name']}"
    if not os.path.exists(post_folder):
        os.makedirs(post_folder)
    metadata_file = f"{post_folder}/{result['name']}.yml"
    with open(metadata_file, "w") as f:
        f.write(yaml.dump(result))
    print("Created metadata! Now you just gotta solve the problem...")

def add_new_key(name, options, result):
    if name.startswith("@"):
        dependency, rest = name[1:].split(".")
        required_val, actual_name = rest.split(":")
        name = actual_name
        if result[dependency] != required_val:
            return
    result[name] = process_metadata_key(name, options, result)
 
def process_metadata_key(name, options, result):
    if options.get("list"):
        items = []
        while True:
            add_another = input(f"{options['prompt']}? y/n: ")
            if add_another == "y":
                item_dict = {}
                for key, val in options["list_keys"].items():
                    add_new_key(key, val, item_dict)
                items.append(item_dict)
            if add_another == "n":
                break
        return items

    assert type(options) == dict


    if options.get("parent"):
        options.pop("parent")
        return process_metadata_dict(options)
    prompt = options["prompt"] + ":  "
    if "choices" in options:
        terminal_menu = TerminalMenu(options["choices"], title=prompt)
        choice_index = terminal_menu.show()
        answer = options["choices"][choice_index]
        print(prompt + answer)
        return answer
    if options.get("autogenerate"):
        generator = config.AUTOGENERATORS[name]
        args = [result[arg] for arg in generator.args]
        return rlinput(prompt, generator.func(*args))
    return input(prompt)

def process_metadata_dict(d):
    result = {}
    for key, val in d.items():
        add_new_key(key, val, result)
    return result

def choose_category():
    prompt = "Choose category: "
    category_options = config.categories_list() + ["New Category"]
    terminal_menu = TerminalMenu(category_options, title=prompt)
    choice_index = terminal_menu.show()
    category = category_options[choice_index]
    if category == "New Category":
        category = input("New category (slug-case):  ")
    print(prompt + category)
    return category
