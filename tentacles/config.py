"""
Folder structure like:

-algorithms
--category_1
----post_1
-----post_1.py
-----post_1.md
-----post_1.yml
----post_2
-----post_2.js
-----post_2.md
-----post_2.yml
--category_2
----post_3
-----post_3.js
-----post_3.md
-----post_3.yml

Solution file, YAML, and Markdown filenames must match the name of the enclosing folder.
"""
import os

from tentacles.utils.strings import function_name_to_slug, slug_to_title

class AutoGenerator:
    def __init__(self, func, args):
        """args is a list of keys in the metadata that this requires"""
        self.func = func
        self.args = args

class Config:
    EXCLUDE = ['.DS_Store', 'TEMPLATE']

    def exclude(self, iterable):
        return list(filter(lambda x: x not in self.EXCLUDE, iterable))

    ALGORITHMS_FOLDER = './content/algorithms'
    DEST_FOLDER = './front-end/src'
    POSTS_DEST = f'{DEST_FOLDER}/posts'
    DATA_STRUCTURES_SOURCE_FOLDER = './content/data-structures'
    DATA_STRUCTURES_DEST = f'{DEST_FOLDER}/data-structures'

    # React App Templates
    REACT_TEMPLATES_FOLDER = f'./templates/front-end'
    POST_TEMPLATE = f'{REACT_TEMPLATES_FOLDER}/Post.jsx'
    ROUTES_TEMPLATE = f'{REACT_TEMPLATES_FOLDER}/Routes.jsx'
    NAV_TEMPLATE = f'{REACT_TEMPLATES_FOLDER}/Nav.jsx'
    API_CALL_TEMPLATE = f'{REACT_TEMPLATES_FOLDER}/apiCall.js'
    BT_PLAYGROUND_TEMPLATE = f'{REACT_TEMPLATES_FOLDER}/BinaryTreePlayground.jsx'
    # React Snippets
    REACT_SNIPPETS_FOLDER = f'{REACT_TEMPLATES_FOLDER}/snippets'
    ROUTE_SNIPPET = f'{REACT_SNIPPETS_FOLDER}/Route.jsx'
    NAV_ITEM_SNIPPET = f'{REACT_SNIPPETS_FOLDER}/NavItem.jsx'
    NAV_CATEGORY_SNIPPET = f'{REACT_SNIPPETS_FOLDER}/NavCategory.jsx'
    IMPORT_SNIPPET = f'{REACT_SNIPPETS_FOLDER}/import.js'

    # Flask Templates
    PYTHON_API_DEST = './api/py'
    FLASK_APP_DEST = f'{PYTHON_API_DEST}/flask_app.py'
    FLASK_APP_REQUIREMENTS_DEST = f'{PYTHON_API_DEST}/requirements.txt'
    # We are using the same reqs for the main app and the api, for simplicity.
    PYTHON_REQUIREMENTS = './requirements.txt'
    FLASK_TEMPLATES_FOLDER = f'./templates/py'
    FLASK_APP_TEMPLATE = f'{FLASK_TEMPLATES_FOLDER}/flask_app.py'
    FLASK_ROUTE_SNIPPET = f'{FLASK_TEMPLATES_FOLDER}/snippets/route.py'

    # Go Templates
    GO_APP_DEST = './api/go'
    GO_MAIN_DEST = f'{GO_APP_DEST}/main.go'
    GO_TEMPLATES_FOLDER = './templates/go'
    GO_MAIN_TEMPLATE = f'{GO_TEMPLATES_FOLDER}/main.go'
    GO_SNIPPETS_FOLDER = f'{GO_TEMPLATES_FOLDER}/snippets'
    GO_ROUTE_SNIPPET = f'{GO_SNIPPETS_FOLDER}/route.go'
    GO_ROUTE_REGISTRATION_SNIPPET = f'{GO_SNIPPETS_FOLDER}/routeRegistration.go'


    # Placeholders
    PH_START = '<3>'
    PH_END = '</3>'

    METADATA_TEMPLATE = "./templates/metadata.yml"
    AUTOGENERATORS = {
        "slug": AutoGenerator(function_name_to_slug, ["language", "name"]),
        "title": AutoGenerator(slug_to_title, ["slug", ])
    }

    def categories_list(self):
        categories = self.exclude(os.listdir(self.ALGORITHMS_FOLDER))
        category_paths = map(lambda d: f"{self.ALGORITHMS_FOLDER}/{d}", categories)

        return [
            categories[i]
            for i, cat in enumerate(category_paths)
            if os.path.isdir(cat)
        ]

    def category_path(self, path):
        return self.ALGORITHMS_FOLDER + "/" + path

config = Config()
