## Naive solution
This would have been very easy if we could do O(n + m) time. We could have just checked the lengths of the arrays, and then used pointers to iterate through both arrays simultaneously until we find the median.

## The approach
I attended a lecture where my mentor solved this problem using the same approach I use below in terms of functionality, with a single function. His approach was roughly the following:

If we need O(log(n + m)) time, we have to use binary search. This basically requires this approach:

1. Guess where the median is.
2. Return the answer if your guess was good.
3. Otherwise, based on what your guess turned up, choose where to make your next guess.

That's easy when you have one data structure. But with two, it's more complicated. Where do we guess? How do we know if our answer was right? And if it was wrong, how do we know what direction to go?

Here's a basic set of steps for what my mentor proposed:

1. Guess the middle of the longer array.
2. Check to see if that created an "interleaving window" in the shorter array.
3. Modify the upper and lower bounds based on the direction indicated if we did not find an interleaving window.

An interleaving window is a set of 4 indices:

- 2 adjacent numbers in either array (since it's sorted, they are sorted)
- the smaller number from each array is also smaller than the bigger number in the other array

## My goal
I thought it was a fascinating approach, but found it difficult to follow the code. I decided to reimplement the solution using a declarative style, breaking the problem down into its component parts and writing helper functions to perform all the heavy lifting.
