import React, { Suspense } from 'react'

import {BinaryTree} from 'data-structures';
import {
  Markdown,
  SyntaxHighlighter,
} from 'components'

const Graphviz = React.lazy(() => import('graphviz-react'));

const txtVersion = `<3>IMPLEMENTATION</3>`;

const initialInput = [1,2,3,4,"null",6,7]

const gvOptions = {
	width: 400,
}

function treeFromInput(input) {
	return BinaryTree.fromLevelOrder(input.map(el => el === "null" ? null : el));
}

export default function BinaryTreePlayground() {
	const [input, setInput] = React.useState(initialInput);
	const [btRep, setBtRep] = React.useState(<Graphviz dot={treeFromInput(initialInput).graphVizDOT} options={gvOptions} />);

	function handleInput(e) {
		const {target: { value }} = e;
		const res = value.split(",")
		setInput(res)
	}

	function handleSubmit(e) {
		e.preventDefault();
		try {
			const bt = treeFromInput(input)
			setBtRep(<Graphviz dot={bt.graphVizDOT} options={gvOptions} />)
		} catch (e) {
			setBtRep(<div style={{height: 500, width: 300, display:"flex", alignItems: "center", justifyContent: "center"}}><p>{e.message}</p></div>)
		}
	}

	return (
		<main className="main">
			<div style={{
				display: "flex",
				"justifyContent": "space-between",
				alignItems: "center",
				width: "100%",
				maxWidth: 1200,
			}}>
			<form onSubmit={handleSubmit} style={{
				width: "40%",
			}}>
				<h1>Make Yer Own Binary Tree!</h1>
						<label htmlFor="level-order-input" style={{"fontSize": "18px"}}>
							Enter a level-order traversal.
							Use &quot;null&quot; for an empty node.
						</label>
						<input
							style={{
								width: "100%",
								display: "block",
								"backgroundColor": "#000",
								border: "1px dashed #008F11",
								color: "#008F11",
								padding: "5px",
								"fontSize": "16px",
							}}
							id="level-order-input"
							value={input.join(",").trim()}
							onChange={handleInput}
						/>
					<button
						style={{
							display: "block",
							"backgroundColor": "hotpink",
							border: "none",
							width: "100%",
							"fontSize": "20px",
							"marginTop": "15px",
						}}
						type="submit"
					>
						Submit
					</button>
			</form>
			<Suspense fallback={<div>Loading...</div>}>{btRep}</Suspense>
			</div>
			<h2>Sneak peek into the implementation behind this page.</h2>
			<Markdown>The result of `graphVizDOT` is fed into [graphviz-react](https://www.npmjs.com/package/graphviz-react)</Markdown>
			<SyntaxHighlighter language="js">{txtVersion}</SyntaxHighlighter>
		</main>
	);
}
