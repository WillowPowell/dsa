type <3>PASCAL_NAME</3>Request struct {
	<3>REQUEST_STRUCT_ARGS</3>
}

func <3>PASCAL_NAME</3>Route(c *gin.Context) {
	var request <3>PASCAL_NAME</3>Request
	if err := c.ShouldBind(&request); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}
	result := <3>name</3>(<3>FUNCTION_CALL_ARGS</3>)
	c.JSON(200, gin.H{"answer": &result})
}

<3>SOLUTION</3>
